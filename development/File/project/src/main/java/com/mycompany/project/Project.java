package com.mycompany.project;
import java.io.*;
//@author Emerson Espinoza <emerson.espinoza.s@uni.pe>

public class Project {

    public static void main(String[] args) {
        
        File archivo = new File("ejemplo_archivo");
        /*File(String,pathname) si no se coloca el pathname es decir la ruta 
        se crea es archivo con nombre "ejemplo_archivo" en la ruta del proyecto
        C:/......
        */
        
        //veamos donde se guardo mi archivo usando el metodo .getAbsolutePath()
        System.out.println(archivo.getAbsolutePath());
        /*vemos que la ruta es C:\Users\USUARIO\development\File\project\ejemplo_archivo
        la cual es la ruta de mi proyecto actual*/
        
        //veamos si este archivo existe por el momento o no en esa ruta usando el metodo .exists();
        System.out.println(archivo.exists());//false
        //vemos que en nuestra ruta si existen otros archivos , comprobemoslo con el metodo .exists();
        /*
        File archivo1 = new File("src");
        System.out.println(archivo1.getAbsolutePath());
        C:\Users\USUARIO\development\File\project\src
        System.out.println(archivo1.exists());
        true
        */
        
        //pureba:crearemos un programa que verifique y muestre que hay dentro de una ruta especifica
        //veamoslo para la ruta C:\Users\USUARIO\Desktop\proyect , creamos una carpeta proyect en escritorio , esta es su ruta
        
        File ruta = new File("C:"+File.separator+"Users"+File.separator+"USUARIO"+File.separator+"Desktop"+File.separator+"proyect");
        System.out.println(ruta.getAbsolutePath());
        //metodo .list(); devuelve un array de String con los nombres y directorios que se encuentran dentro de la ruta
        String[] nombres_archivos = ruta.list();
        //recorremos ese array
        for(int i=0;i<nombres_archivos.length;i++)
        {
            System.out.println(nombres_archivos[i]);
            
            File f = new File(ruta.getAbsolutePath(),nombres_archivos[i]);//argumentos(Ruta padre, Ruta hijo)
            if(f.isDirectory())//si f es un directorio
            {
                String[] archivos_subcarpeta=f.list();//para ver los archivos y directorios de f
                for(int j=0;j<archivos_subcarpeta.length;j++)
                {
                    System.out.println(archivos_subcarpeta[j]);
                }
            }
         }
        //en esa carpeta habian 3 archivos de texto(segun yo lo cree) y una carpeta(Nueva Carpeta)
        //como accedo a esa carpeta desde la ruta ya dada anteriormente?
        //utilizando un metodo .isDirectory();esto indica si el archivo es un directorio o no
        //en el caso que lo sea ENTRA y listar todo lo que pueda haber en su interior
        
        
        
        
    }
}
