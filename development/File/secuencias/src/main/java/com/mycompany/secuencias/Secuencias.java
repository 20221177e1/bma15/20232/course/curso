package com.mycompany.secuencias;
import java.io.*;
//@author Emerson Espinoza <emerson.espinoza.s@uni.pe>

public class Secuencias {

    public static void main(String[] args) throws IOException {
        
        //secuencia de caracteres o bytes
        //bytes para enviar informacion a un lugar remoto,mas conveniente
        //caracteres cuando necesitemos leer la informacion
        //en secuencia de caracteres usamos las clases Writer y Reader , clases del package java.io
        
        //usando los pasos anteriores creamos u ntexto txt en una ruta
        //PARA LA RUTA
        File ruta = new File("C:"+File.separator+"Users"+File.separator+"USUARIO"+File.separator+"Desktop"+File.separator+"secuencias");
        File archivo = new File(ruta.getAbsolutePath()+File.separator+"texto.txt");
        archivo.createNewFile();
        
        //se creo el archivo de texto , como primero haremos la lectura , en este archivo creado escribiremos frases de forma manual
        //posteriormente se explicara como escribir texto desde el iDE
        FileReader leer_archivo = new FileReader(archivo.getAbsolutePath());
        int c=leer_archivo.read();//se almacena el codigo del primer caracter en el fichero de texto , se almacena en c
        StringBuilder text = new StringBuilder();
        while(c!=-1)
        {
             c=leer_archivo.read();
             char letra = (char)(c);
             if(c!=-1 && c!=0)//ya no se imprime la ultima "?"
             {
                 text.append(letra);
                 System.out.print(letra);
             }
        }
        System.out.println("");
        System.out.println(text);//ahora tenemos almacenada en una variable StringBuilder el texto en el archivo
        //entonces podemos usar diferentes metodos segun veamos conveniente
        
        
        
    }
}
