/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.mavenproject1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

/**
 *
 * @author Emerson Espinoza <emerson.espinoza.s@uni.pe>
 */
public class Mavenproject1 {

    public static void main(String[] args) throws FileNotFoundException, IOException {
        System.out.println("Byte Stream with files");
        //Create a File
        String name ="ByteStream.txt";
        File file = new File(name);
        // write 
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(74);
        fileOutputStream.write(85);
        fileOutputStream.write(76);
        fileOutputStream.write(73);
        fileOutputStream.write(65);
        fileOutputStream.close();
        
        System.out.println("Location"+file.getAbsolutePath());
        
        //Read a File
        FileInputStream fileInputStream = new FileInputStream(file);
        int decimal;
        while((decimal = fileInputStream.read())!=-1)
        {
            System.out.print((char)(decimal));
        }
        System.out.println("");
        
        name = "CharacterStream.txt";
        file = new File(name);
        //write 
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write(74);
        fileWriter.write(85);
        fileWriter.write(76);
        fileWriter.write(73);
        fileWriter.write(65);
        
        fileWriter.close();
        
        System.out.println("FileWriter Location: "+file.getAbsolutePath());
        //read
        FileReader fileReader = new FileReader(file);
        while((decimal=fileReader.read())!=-1)
        {
            System.out.print((char)(decimal));
        }
        System.out.println("");

        
        System.out.println("List of files");
        String[] paths;
        file = new File(".");
        paths = file.list();
        System.out.println(Arrays.toString(paths));
        
        System.out.println("Creating directories");
        System.out.println("Absolute path: "+file.getAbsolutePath());
        String directory = "Files/Binaries/Selected";
        String fullPath = file.getAbsolutePath() + directory;
        file = new File(fullPath);
        if(file.mkdirs())
        {
            System.out.println("Directories have be created");
        }
        else
        {
            System.out.println("Directories have already been created");
        }
        
        
        
        
                
    }
}
