package com.mycompany.clases;

/**
 *
 * @author Emerson Espinoza <emerson.espinoza.s@uni.pe>
 */
public class Libro {
    
    //atributos para mi clase libro
    //notar que no les asignamos valores 
    private int numPaginas;
    private String titulo;
    private String autor;
    private int ISBN;
    
    public Libro(int pISBN, String pAutor, String pTitulo, int pNumPaginas)
    {
         ISBN=pISBN;
         autor=pAutor;
         titulo=pTitulo;
         numPaginas=pNumPaginas;
    }

    public int getNumPaginas() {
        return numPaginas;
    }

    public void setNumPaginas(int numPaginas) {
        this.numPaginas = numPaginas;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public int getISBN() {
        return ISBN;
    }

    public void setISBN(int ISBN) {
        this.ISBN = ISBN;
    }
    
    @Override
    public String toString()
    {
        return ("El libro"+titulo+"que tiene por ISBN"+ISBN+"y tiene por autor"+autor+"tiene"+numPaginas+"paginas");
        
    }
}
