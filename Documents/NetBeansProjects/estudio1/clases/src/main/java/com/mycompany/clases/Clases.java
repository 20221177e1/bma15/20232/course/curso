/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.clases;

/**
 *
 * @author Emerson Espinoza <emerson.espinoza.s@uni.pe>
 */
public class Clases {

    public static void main(String[] args) {
        
        Libro libro1=new Libro(111111,"titulo1","autor1",30);
        Libro libro2=new Libro(111112,"titulo2","autor2",60);
        
    
        //mostrar
        System.out.println(libro1.toString());
        System.out.println(libro2.toString());
        

        //indicar cual libro tiene mas paginas
        if(libro1.getNumPaginas()>libro2.getNumPaginas())
        {
            System.out.println("Las paginas del libro"+libro1.getTitulo()+"tiene mas paginas que "+libro2.getTitulo());
            
        }
        else
        {
            System.out.println("Las paginas del libro"+libro2.getTitulo()+"tiene mas paginas que "+libro1.getTitulo());
        }
        
    }
}
