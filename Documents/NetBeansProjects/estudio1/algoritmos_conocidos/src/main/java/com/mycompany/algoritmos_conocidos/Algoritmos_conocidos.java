/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.algoritmos_conocidos;
import java.util.Scanner;
import java.util.Random;
/**
 *
 * @author Emerson Espinoza <emerson.espinoza.s@uni.pe>
 */
public class Algoritmos_conocidos {

    public static void main(String[] args) {
        
        Random rand = new Random();
        Scanner input = new Scanner(System.in);
        
        //FACTORIAL DE UN NUMERO
        int fact=1;
        int num=5,aux;
        aux=num;
        
        while(num>0)
        {
            fact = fact*num;
            num--;
        }
        System.out.println("Factorial de "+aux+" es: "+fact);
        //DETECTAR LA CIFRA MAYOR y MENOR
        int r,num1=12345,mayor=0,menor=9;//asumimos una mayor/menor cifra
        int aux2;
        
        aux2=num1;
        while(num1!=0)
        {
            r=num1%10;
            num1=num1/10;
            
            if(r>=mayor)
            {
                mayor=r;
            }
            if(r<=menor)
            {
                menor=r;
            }
        }
        System.out.println("El numero "+aux2+" tiene como cifra mayor: "+mayor+" y como cifra menor: "+menor);
            
        //INVERSO DE UN NUMERO abcd-->dcba
        int num3=12345,numi=0,aux3;
        aux3=num3;
        while(num3!=0)
        {
            r=num3%10;
            num3=num3/10;
            numi=numi*10+r;
        }
        System.out.println("El inverso de "+aux3+" es: "+numi);
        //ALMACENAR LAS CIFRAS EN UN ARREGLO
        int num4=123456789;
        int longitud=1+(int)(Math.log10(num4));
        
        int dat[]=new int [longitud];
        int i=0;
        while(num4!=0)
        {
            r=num4%10;
            num4=num4/10;
            
            dat[i]=r;
            i++;
        }
        System.out.println("Cifras:");
        for(i=0;i<longitud;i++)
        {
            System.out.print(""+dat[i]+",");
        } 
        //DETECTAR SI EL NUMERO ES PRIMO
        int num5,cont=0;
        
        System.out.println("");
        do{
            System.out.println("Ingrese un numero entero positivo:");
            num5=input.nextInt();
        }while(num5<=0);
        
        for(i=1;i<=num5;i++)
        {
            if(num5%i==0)
            {
                cont++;
            }
        }
        if(cont==2)
        {
            System.out.println("El numero es primo");
        }
        if(cont!=2)
        {
            System.out.println("El numero no es primo");
        }
        
        //DETECTAR SI EL NUMERO ES CAPICUA
        int num7,longitud2;
        while(true)
        {
            System.out.println("Ingrese un numero entero positivo:");
            num7=input.nextInt();
            if(num7>=0)
            {
                break;
            }
        }
        int aux5;
        longitud2= 1 + (int)(Math.log10(num7));
        
        i=0;
        int cifras[]=new int [longitud2];
        aux5=num7;
        while(num7!=0)
        {
            r=num7%10;
            num7=num7/10;
            
            cifras[i]=r;
            i++;
        }
        System.out.println("Cifras");
        for(i=0;i<longitud2;i++)
        {
            System.out.print(""+cifras[i]+",");
        }
        System.out.println("");
        int a2,cont2=0;
        if(longitud2%2==0)
        {
            a2=longitud2/2;
            for(i=0;i<a2;i++)
            {
                if(cifras[i]==cifras[longitud2-1-i])
                {
                    cont2++;
                }
            }
            if(cont2==a2)
            {
                System.out.println("El numero es capicua");
            }
        }
        if(longitud2%2!=0)
        {
            a2=(longitud2-1)/2;
            for(i=0;i<a2;i++)
            {
                if(cifras[i]==cifras[longitud2-1-i])
                {
                    cont2++;
                }
            }
            if(cont2==a2)
            {
                System.out.println("El numero es capicua");
            }
        }
        
        //GENERAR DATOS ALEATORIOS Y GUARDARLOS EN UN ARREGLO
        int dat10[]=new int[7];
        mayor=dat10[0];
        menor=dat10[0];
        for(i=0;i<7;i++)
        {
            //RANDOM EN INTERVALO [A,B] sea A=10 B=20;
            dat10[i]=10+rand.nextInt(20-10+1);
            if(dat10[i]<=menor)
            {
                menor=dat10[i];
            }
            if(dat10[i]>=mayor)
            {
                mayor=dat10[i];
            }
            System.out.print(""+dat10[i]+",");
        }
        System.out.println("mayor:"+mayor+" menor:"+menor);//se imprimira en menor 0
        //porque en el algoritmo recien se estan creando los valores portando dat10[0] al no
        //asignarme valores aun al rreglo , los elementos de este arreglo son ceros.
        /*
        ORDENAR DE MAYOR A MENOR
        for(i=0;i<ndat-1;i++)
        {
            for(j=0;j<ndat-1-i;j++)
            {
                if(dat[j] < ó > dat[j+1])
                temp=dat[j];   
                dat[j]=dat[j+1];
                dat[j+1]=temp;
            }
        }
        */
        /*
         for (i = 0; i < n; i++) {
        do {
            isDuplicate = 0;
            num = rand() % 100; // Genera números aleatorios en este ejemplo (0-99)
            // Verifica si el número generado ya está en el arreglo
            for (j = 0; j < i; j++) {
                if (arr[j] == num) {
                    isDuplicate = 1;
                    break;
                }
            }
        } while (isDuplicate);

        arr[i] = num;
    }

    // Imprime el arreglo resultante
    for (i = 0; i < n; i++) {
        printf("%d ", arr[i]);
    }

    return 0;
}
        */
      
    }
}
