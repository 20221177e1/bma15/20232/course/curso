/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.mavenproject1;
import java.util.Scanner;
import java.util.Random;

/**
 *
 * @author Emerson Espinoza <emerson.espinoza.s@uni.pe>
 */
public class Mavenproject1 {

    public static void main(String[] args) {
        
        int num1=5,num2=7,result;
        boolean condicion1,condicion2,condicion3;
        
        condicion1=num1>num2;
        condicion2=num2>num1;
        condicion3=num1==num2;
        
        result = (condicion1? 1:0)*num1 + (condicion2? 1:0)*num2 + (condicion3? 1:0)*num1;//(condicion? 1:0) se le asigna unvalor 
        System.out.println("El mayor es:"+result);
        
        //ahoracon condicionales
        if(num1>num2)
        {
            System.out.println("el numero "+num1+" es mayor que "+num2);
        }
        if(num1<num2)
        {
            System.out.println("el numero "+num2+" es mayor que "+num1);
        }
        if(num1==num2)
        {
            System.out.println("el numero "+num1+" es igual que "+num2);
        }
        //ahora con swich
        condicion1=num1>num2;
        int aux;
        aux= (condicion1? 1:0);//asignacion
        switch(aux)
        {
            case 1: System.out.println("El numero "+num1+"es mayor que "+num2);
                    break;
            case 0: System.out.println("El numero "+num2+" es mayor que "+num1);
                    break;
            default:System.out.println("Los numeros son iguales");//linea de codigo que se ejecuta en caso case 1 y case 0 no sean ciertos
                    break;
        }
        //while
        int i=0;
        while(i<=10)
        {
            System.out.print(""+i+",");
            i++;
        }
        //import java.util.Scanner;
        // Scanner input = new Scanner(System.in);
        Scanner input = new Scanner(System.in);
        int variable;
        
        System.out.println("");
        variable = input.nextInt();
        System.out.println("numero:"+variable);
    
        //for
        for(i=0;i<10;i++)
        {
            System.out.println(""+i);
        }
        //arreglos unidimensionales --vectores
        //int ndat tamaño de mi arreglo
        //int dat[] = new int [ndat]
        int ndat;
        ndat=input.nextInt();
        int dat[]=new int [ndat];//tamaño del arreglo
        for(i=0;i<ndat;i++)
        {
            System.out.println("Ingrese el valor de dat["+i+"]");
            dat[i]=input.nextInt();
            System.out.print("\ndat["+i+"] = "+dat[i]+"\n");
        }
        //numeros random
        //import java.util.random
        
        Random rand = new Random();
        int numero;
        numero= 1+rand.nextInt(10-1+1);//aleatorio entre 0 y un numero anterior al numeor entre();
        //si quiero que este en cierto intervalo [min,max] tendriamos que hacer
        //numero = min + rand.nextInt(max-min+1)
        System.out.println(""+numero);
        //resumen
        //import java.util.Random
        //Random rand = new Random();
        //generar aleatorio enjtre [a,b]
        // int numero= min + rand(max-min+1);
        
        
        //clase math
        double numero2=Math.PI;//Math.PI es el numero pi , analogamente Math.E es el numero de euler 2.71....
        //debe ser almacenada en variable double
        System.out.println(""+numero2);
        //potencia
        //Math.pow(a,b) a:base , b:potencia
        double var;
        var=Math.pow(4,0.5);//la variable usada en el metodo Math debe ser double b puede ser float
        //si se quiere volver entero la expresion hacemos (int)
        
        System.out.println(""+(int)var);
        //redondedos Math.round
        
        
        //STRING
        String nombre="Emerson";
        //acceder a un caracter por medio de .charAt(posicion)
        //ejemplo:
        System.out.println(""+nombre.charAt(5));
        //contar la cantidad de elementos del array
        System.out.println(""+nombre.length());
        //sacar un subfrase
        //.substring(x1,x2) de la posicion x1 a x2-1 
        System.out.println(""+nombre.substring(1, 3));
        //replace
        //.replace(" ","-") se reemplaza espacios por guiones 
        //comparar dosvalores de caracter
        //equals
        //nombre.equals(variable) donde nombre y variable corresponden a tipo String
        //retorna True si nombre == variable , caso contrario False , si se quiere asignar 1 o 0
        //para calculos matematicos almacenamos este equals en una variable tipo boolean
        // boolean condicion = nombre.equals(variable)
        // int valor = (condicion? 1:0);
        
        int longitud,num=12345;
        longitud = 1+ (int)(Math.log10(num));
        System.out.println("longitud:"+longitud);
    }
}
