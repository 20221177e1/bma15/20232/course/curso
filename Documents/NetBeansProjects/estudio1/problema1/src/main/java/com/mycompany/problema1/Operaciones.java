
package com.mycompany.problema1;
import java.util.Scanner;

/**
 *
 * @author Emerson Espinoza <emerson.espinoza.s@uni.pe>
 */
public class Operaciones {
    /*ATRIBUTOS*/
    private int numero1;
    private int numero2;
    private int suma;
    private int resta;
    private int multiplicacion;
    private double division;
    
    //METODOS
    public void leerNumeros(){
        Scanner input=new Scanner(System.in);
        
        while(true)
        {
            System.out.println("Ingrese un numero entero positivo:");
            numero1=input.nextInt();
            System.out.println("Ingrese un numero entero positivo:");
            numero2=input.nextInt();
            if(numero1>0 && numero2>0)
            {
                break;
            }
        } 
    }
    public void sumar()
    {
        suma=numero1+numero2;
    }
    public void restar()
    {
        if(numero1>=numero2)
        {
            resta=numero1-numero2;
        }
        else
        {
            resta=numero2-numero1;
        }
            
    }
    public void multiplicar()
    {
        multiplicacion=numero1*numero2;
    }
    public void dividir()
    {
        if(numero1>=numero2)
        {
            division=numero1/numero2;
        }
        else
        {
            division=numero2/numero1;
        }
    }
    public void mostrarResultados()
    {
        System.out.println("La suma es:"+suma);
        System.out.println("La resta es:"+resta);
        System.out.println("La multiplicacion es:"+multiplicacion);
        System.out.println("La division es:"+division);
    }
}
