package com.mycompany.problema1;
//@author Emerson Espinoza <emerson.espinoza.s@uni.pe>
 
public class Problema1 {

    public static void main(String[] args) {
        
        Operaciones op = new Operaciones();
        
        op.leerNumeros();
        op.sumar();
        op.restar();
        op.multiplicar();
        op.dividir();
        op.mostrarResultados();
        
        
    }
}
