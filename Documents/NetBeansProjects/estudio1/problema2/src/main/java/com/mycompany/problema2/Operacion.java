//@author Emerson Espinoza <emerson.espinoza.s@uni.pe>
package com.mycompany.problema2;
public class Operacion {
    
    private int suma;
    private int resta;
    private int multiplicacion;
    private double division;
    
    public void sumar(int numero1,int numero2)
    {
        suma=numero1+numero2;
        
    }
    public void restar(int numero1,int numero2)
    {
        resta=numero1-numero2;
        
    }
    public void multiplicar(int numero1,int numero2)
    {
        multiplicacion=numero1*numero2;
        
    }
    public void dividir(int numero1,int numero2)
    {
        division=numero1/numero2;
        
    }

    
    
}
