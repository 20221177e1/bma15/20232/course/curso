package com.mycompany.problema2;
//@author Emerson Espinoza <emerson.espinoza.s@uni.pe>
import java.util.Scanner;

public class Problema2 {

    public static void main(String[] args) {
        Scanner input= new Scanner(System.in);
        
        int n1,n2;
        n1=input.nextInt();
        n2=input.nextInt();
        
        Operacion op=new Operacion();
        
        op.sumar(n1,n2);
        op.restar(n1,n2);
        op.multiplicar(n1,n2);
        op.dividir(n1,n2);
        
        
        
        
    }
}
