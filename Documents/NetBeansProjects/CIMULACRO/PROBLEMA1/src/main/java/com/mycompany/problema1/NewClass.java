package com.mycompany.problema1;

/**
 *
 * @author Emerson Espinoza <emerson.espinoza.s@uni.pe>
 */
import java.util.Random;
import java.util.Scanner;

public class NewClass {
    Random rnd = new Random();
    Scanner input= new Scanner(System.in);

    private int aleatorio;
    private int tamaño;
    
    public void datosAleatorio()
    {
        //genera valores aleatorios entre 10 y 99
        int i,N;
        do{
            System.out.println("Ingrese el numero de datos a generar");
            N=input.nextInt();
            
        }while(N<=0);
        for(i=0;i<N;i++)
        {
            aleatorio = 10+rnd.nextInt(99-10+1);
            System.out.print(""+aleatorio+",");
        }
        
    }
    public void eliminarDato()
    {
        while(true)
        {
            System.out.println("Ingrese el tamaño del arreglo:");
            tamaño=input.nextInt();
            if(tamaño>0)
            {
                break;
            }
        }
        //se obtuvo el tamaño correcto ahoracreamos un arreglo de ese tamaño
        int dat[]=new int [tamaño];
        int i,j;
        //el usuario ingrese valores , no importa si se repiten y lo imprimimos
        for(i=0;i<tamaño;i++)
        {
            System.out.println("Ingrese valores:");
            dat[i]=input.nextInt();
        }
        //eliminacion de los repetidos
        for(i=0;i<tamaño;i++)
        {
            for(j=0;j<tamaño;j++)
            {
                while(i!=j && dat[i]==dat[j])
                {
                    dat[i]=1+rnd.nextInt(20-1+1);
                    j=0;
                }
            }
        }
        //imprimimos el nuevo arreglo si nrepticiones
        for(i=0;i<tamaño;i++)
        {
            System.out.print(""+dat[i]+",");
        }
        
         
    }
    public void convierteTOmatriz()
    {
        int ndat;
        do{
            System.out.println("Ingrese el tamaño del arreglo:");
            ndat=input.nextInt();
        }while(ndat<=0);
        int dat1[]=new int [ndat];
        int dat2[][]= new int [ndat][ndat];
        int i,j;
        for(i=0;i<ndat;i++)
        { 
            dat1[i]=10+rnd.nextInt(99-10+1);
            System.out.print(""+dat1[i]+",");
        }
        System.out.println("");
        for(i=0;i<ndat;i++)
        {   
            for(j=0;j<ndat;j++)
            {
                dat2[i][j]=dat1[j];
                System.out.print(""+dat2[i][j]+"\t");
            }
            System.out.println("");
        }
       }
}
