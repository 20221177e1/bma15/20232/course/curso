package com.mycompany.problema1;

/**
 *
 * @author Emerson Espinoza <emerson.espinoza.s@uni.pe>
 */
public class PROBLEMA1 {

    public static void main(String[] args) {
        NewClass nw = new NewClass();
        nw.datosAleatorio();
        System.out.println("");
        nw.eliminarDato();
        System.out.println("");
        nw.convierteTOmatriz();
    }
}
