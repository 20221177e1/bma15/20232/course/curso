package com.mycompany.problema2;
import java.util.Scanner;

/**
 *
 * @author Emerson Espinoza <emerson.espinoza.s@uni.pe>
 */
public class ALUMNO {
    
    Scanner input=new Scanner(System.in);
    
    private String codigo;
    private String apellido;
    private String fecha;
    
    public String leer_codigo()
    {
        System.out.println("Ingrese su codigo:");
        codigo=input.nextLine();
        return codigo;
        //s
    }
    public String leer_apellido()
    {
        System.out.println("Ingrese su apellido:");
        apellido=input.nextLine();
        return apellido;
    }
    public String leer_fecha()
    {
        System.out.println("Ingrese la fecha de ingreso(ejem:17/10/2022):");
        fecha=input.nextLine();
        return fecha;
    }
}
