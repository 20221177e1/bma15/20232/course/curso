package com.mycompany.stringbuilder;

/**
 *
 * @author Emerson Espinoza <emerson.espinoza.s@uni.pe>
 */
public class Stringbuilder {

    public static void main(String[] args) {
       
        String texto="Hola";
        texto=texto+" Como estas";
        
        System.out.println(texto);//las variables string son inmutables
        //es decir no pueden ser cambiadas despues
        //dicho de otra forma la capacidad como el tamaño son iguales , si trabajamos con una variable tipo String
        
        
        //StringBuilder
        StringBuilder textoBuilder= new StringBuilder();//instanciamos el objeto
        textoBuilder.append("Hola como estas").append(" Saludos desde ").append("Lima Peru");
        //usando el metodo append a un textoBuilder podemos "concadenar" , luego hacemos la impresion:
        System.out.println(textoBuilder);
        
        //ejemplo2
        
        long tiempo_inicio = System.nanoTime();//metodo para obtener la hora del sistema en formato nanosegundos
        
        StringBuilder builder = new StringBuilder();
        
        
        for(int i=0;i<100;i++)
        {
            builder.append(i);//
        }
        long tiempo_fin = System.nanoTime();
        
        double diferencia = tiempo_fin - tiempo_inicio;
        System.out.println(diferencia);
        
        
        //con contanacion
        long tiempo_inicio2= System.nanoTime();
        String texto2="";
        for(int i=0;i<100;i++)
        {
            texto2=texto2+i;
        }
        long tiempo_fin2= System.nanoTime();
        double diferencia2=tiempo_fin2 - tiempo_inicio2;
        System.out.println(diferencia2);
        
        //se observa que el tiempo de uso es muchisimo menor usando el stringbuilder
        //que usando la concatenacion , aproximadamente 20 veces mas 
        //entonces es recomendable usar StringBuilder para concadenaciones de String
    }
}
