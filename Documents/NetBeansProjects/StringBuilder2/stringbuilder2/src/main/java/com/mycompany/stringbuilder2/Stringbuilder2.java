/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.stringbuilder2;

/**
 *
 * @author Emerson Espinoza <emerson.espinoza.s@uni.pe>
 */
public class Stringbuilder2 {

    public static void main(String[] args) {
        
        StringBuilder textoBuilder = new StringBuilder();
        
        textoBuilder.append("Hola ");
        textoBuilder.append("Mundo");
        
        System.out.println(textoBuilder);
        System.out.println(textoBuilder.toString()); 
        
        //delete
        StringBuilder sb6 = new StringBuilder("abcdef");
        sb6.deleteCharAt(5);
        System.out.println("sb6: " + sb6);
        sb6.delete(1, 3);
        System.out.println("sb6: " + sb6);
        
        // insert
        StringBuilder sb5 = new StringBuilder("animals");
        sb5.insert(7, "-");
        sb5.insert(0, "-");
        sb5.insert(4, "-");
        System.out.println("sb5: " + sb5);

        boolean condicion1=true;
      
        System.out.println(!condicion1);
        
    }
}
