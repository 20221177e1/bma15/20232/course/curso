package com.mycompany.problema1;

import java.util.Scanner;
/**
 *
 * @author Emerson Espinoza <emerson.espinoza.s@uni.pe>
 */
public class MATRICULA {
    
    Scanner input=new Scanner(System.in);
    
    private String nombre;
    private String codigo;
    private double promedio;
    private int creditos;
    private String curso;
    
    
    public void calculo_nombre()
    {
        System.out.println("Ingrese su nombre:");
        nombre=input.nextLine();
    }
    public void calculo_codigo()
    {
        System.out.println("Ingrese su codigo de estudiante:");
        codigo=input.nextLine();
        
    }
    public void calculo_promedio()
    {
        while (true)
        {
            System.out.println("Ingrese su promedio del ciclo pasado:");
            promedio=input.nextDouble();
            if(promedio>=0 && promedio<=20)
            {
                break;
            }
            
        }
        if(promedio<8)
        {
            creditos = 15;
        }
        if(promedio>=8 && promedio<10)
        {
            creditos = 20;
        }
        if(promedio>=10 && promedio<12)
        {
            creditos = 24;
        }
        if(promedio>=12 && promedio<14)
        {
            creditos = 26;
        }
        if(promedio>=14 && promedio<=20)
        {
            creditos = 28;
        }
    }
    
    public void seleccion_cursos()
    {
        int aux,i,cont=0;
        aux=creditos;
        String dat[]=new String [6];
        String respuesta="SI";
        
        System.out.println("Creditos disponibles: "+creditos);
        System.out.println("CURSOS/CREDITOS/");
        System.out.println("BMA05/  5   /");
        System.out.println("BFI03/  5   /");
        System.out.println("BEG01/  3   /");
        System.out.println("BMA15/  3   /");
        System.out.println("EE306/  3   /");
        System.out.println("BMA10/  3   /");
        while(true)
        {
            System.out.println("Creditos disponibles: "+creditos);
            
            
            System.out.println("Quiere matricularse en un curso? SI/NO");
            respuesta=input.nextLine();
            if(respuesta.equals("SI"))
            {
                System.out.println("Ingrese el curso que desea matricularse:");
                curso=input.nextLine();
                if(curso.equals("BMA05"))
                {
                    System.out.println("Creditos antes:"+creditos+"Creditos disponibles:"+(creditos-5));
                    creditos=creditos-5;
                    dat[cont]="BMA05";
                    cont++;
                }
                if(curso.equals("BFI03"))
                {
                    System.out.println("Creditos antes:"+creditos+"Creditos disponibles:"+(creditos-5));
                    creditos=creditos-5;
                    dat[cont]="BFI03";
                    cont++;
                }
                if(curso.equals("BEG01"))
                {
                    System.out.println("Creditos antes:"+creditos+"Creditos disponibles:"+(creditos-3));
                    creditos=creditos-3;
                    dat[cont]="BEG01";
                    cont++;
                }
                if(curso.equals("BMA10"))
                {
                    System.out.println("Creditos antes:"+creditos+"Creditos disponibles:"+(creditos-3));
                    creditos=creditos-3;
                    dat[cont]="BMA10";
                    cont++;
                }
                if(curso.equals("BMA15"))
                {
                    System.out.println("Creditos antes:"+creditos+"Creditos disponibles:"+(creditos-3));
                    creditos=creditos-3;
                    dat[cont]="BMA15";
                    cont++;
                }
                if(curso.equals("EE306"))
                {
                    System.out.println("Creditos antes:"+creditos+"Creditos disponibles:"+(creditos-3));
                    creditos=creditos-3;
                    dat[cont]="EE306";
                    cont++;
                }
            }
            if(respuesta.equals("NO")||creditos==0)
            {
                System.out.println("MATRICULA REALIZADA EXITOSAMENTE!!");
                System.out.println("RESUMEN--");
                System.out.println("Alumno: "+nombre+" Codigo: "+codigo+" Creditos llevados:"+(aux-creditos));
                System.out.println("CURSOS MATRICULADOS:");
                for(i=0;i<cont;i++)
                {
                    System.out.println(""+dat[i]);
                }
                break;
            }
           
        }
        
        
        
    }
    
}
