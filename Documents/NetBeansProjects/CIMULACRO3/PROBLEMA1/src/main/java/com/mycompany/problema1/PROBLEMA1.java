package com.mycompany.problema1;

/**
 *
 * @author Emerson Espinoza <emerson.espinoza.s@uni.pe>
 */
public class PROBLEMA1 {

    public static void main(String[] args) {
       
        MATRICULA mat = new MATRICULA();
        mat.calculo_nombre();
        mat.calculo_codigo();
        mat.calculo_promedio();
        mat.seleccion_cursos();
    }
}
