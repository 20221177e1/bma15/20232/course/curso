package com.mycompany.proyecto;
import java.util.Scanner;
import java.lang.Math;
/**
 *
 * @author Emerson Espinoza <emerson.espinoza.s@uni.pe>
 */
public class Proyecto {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int clave=123456;
        IngresoDeClave(input,clave);//ALMACENAMOS LA CLAVE EN UNA VARIABLE
        menu(input);
        
    }
    public static void IngresoDeClave(Scanner input,int clave)
    {
        int num;
        System.out.println("Ingrese su clave secreta:");
        while(true)
        {
            num=input.nextInt();
            if((1+(int)(Math.log10(num)))==6 && num==clave)//ALGORITMO QUE INDICA LA CANTIDAD DE CIFRAS DEL NUMERO
            {
                break;//ACABA EL BUCLE LA CANTIDAD DE CIFRAS DE MI CLAVE ES 6 Y ES IGUAL A MI CLAVE
            }
            else
            {
                System.out.println("clave incorrecta,porfavor vuelva a ingresar su clave:");
                //EN CASO NO CUMPLA LA CONDICION SE REPITE EL BUCLE
            }
        }
    }
    public static void menu(Scanner input)
    {
        String respuesta;
        
        //SE MUESTRA EL MENU
        System.out.println("┌────── MENU ──────┐");
        System.out.println("     RETIRO   (R)    ");
        System.out.println("     DEPOSITO (D)  ");
        System.out.println("└───────────────────┘");
        
        while(true)
        {
            System.out.println("Seleccione una operacion(R o D):");
            respuesta=input.nextLine();
            if(respuesta.equals("R")||respuesta.equals("D"))
            {
                break;//SE ACABA EL BUCLE CUANDO SELECCIONE UNA DE LAS OPERACIONES PERMITIDAS
            }
            else{
                System.out.println("Porfavor vuelva a seleccionar una operacion:");
                //SI SE SELECCIONARA OTRA EL BUCLE SE REPITE 
            }
        }
    }
}

