package com.mycompany.app;

//@author Emerson Espinoza <emerson.espinoza.s@uni.pe>
 
public class BCP {
    boolean cardInserted;//atributo
    
    public BCP(){//constructor
        this.cardInserted=false;
    }
    public boolean getCardInserted(){//retorno del valor
        return(this.cardInserted);
    }//metodos
    public void CardInserted(){
        this.cardInserted=true;
    }
    public void CardNotInserted(){
        this.cardInserted=false;
    }
}
