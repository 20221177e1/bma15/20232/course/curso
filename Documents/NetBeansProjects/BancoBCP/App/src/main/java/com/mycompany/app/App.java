package com.mycompany.app;

//@author Emerson Espinoza <emerson.espinoza.s@uni.pe>
 
public class App {

    public static void main(String[] args) {
        BCP bcp = new BCP();
        LoadingScreen lds = new LoadingScreen();
        Menu1 mn = new Menu1();
        
        System.out.println(bcp.getCardInserted());//inicialmente no esta insertada
        
        bcp.CardInserted();//insertamos la tarjeta
        System.out.println(bcp.getCardInserted());//tarjeta insertada
        lds.setVisible(true);//hacemos visible la ventana
        
        try {
        Thread.sleep(3000); // pausa durante 3 segundos
        } catch (InterruptedException e) {
        e.printStackTrace();
        }   

        lds.setVisible(false); // cerrar la primera ventana
        
        mn.setVisible(true);//hacemos visible la otra ventana
        bcp.CardNotInserted();//retiramos la tarjeta
        System.out.println(bcp.getCardInserted());
    }
}
