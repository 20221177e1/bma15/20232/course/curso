package com.mycompany.app;
import java.awt.Color;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

//@author Emerson Espinoza <emerson.espinoza.s@uni.pe>

public class LoadingScreen extends JFrame{
    
    public LoadingScreen(){
        setSize(900, 500);
        setLocationRelativeTo(null);
        setTitle("Loading Screen");
        startComponents();
    }
    private void startComponents(){
        JPanel panel= new JPanel();
        panel.setLayout(null);

        panel.setBackground(new Color(0, 42, 141));
        
        this.getContentPane().add(panel);
        
        JLabel etiqueta = new JLabel();
        etiqueta.setText("<html>Espere un momento<br>por favor<br>____</html>");
        etiqueta.setBounds(50,50,250,250);
        etiqueta.setForeground(Color.white);
        etiqueta.setFont(new Font("Helvetica",Font.BOLD,22));
        panel.add(etiqueta);
        
        JLabel etiqueta2 = new JLabel(new ImageIcon("C:/Users/USUARIO/Documents/NetBeansProjects/BancoBCP/App/logobcp.png"));
        JLabel persona = new JLabel(new ImageIcon("C:/Users/USUARIO/Documents/NetBeansProjects/BancoBCP/App/persona.png"));
        etiqueta2.setBounds(700,10,120,80);
        persona.setBounds(350,50,800,419);
        panel.add(etiqueta2);
        panel.add(persona);
        
    }
}
